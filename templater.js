const inquirer = require('./node_modules/inquirer');
const fs = require('fs');
const path = require('path');
const process = require('process');
const CWD = process.cwd();
const { success, error } = require('./node_modules/cli-msg');
const Filehound = require('filehound');

class Templater {
  constructor() {
    this.themeDirs = [];
    this.templatePath = {
      field: path.join(
              CWD,
              'core',
              'modules',
              'system',
              'templates',
              'field.html.twig'),
      paragraph: path.join(
              CWD,
              'modules',
              'contrib',
              'paragraphs',
              'templates',
              'paragraph.html.twig')
    }
    this.state = {
      templateName: '',
      themeDir: '',
      type: ''
    }
  }

  init () {
    this.validateProjectRoot();
    this.getThemeDirs();
    this.promptDestination();
  }

  getThemeDirs () {
    const subDirs = Filehound.create()
      .path(path.join(CWD, 'themes'))
      .depth(1)
      .directory()
      .findSync();

    subDirs.forEach(item => {
      this.themeDirs.push(path.parse(item).name);
    });
  }

  validateProjectRoot () {
    try {
      if (!fs.existsSync(path.join(CWD, 'themes'))) {
        error.wb("Make sure you are in a root directory of Drupal");

        process.exit();
      }
    } catch (err) {

    }
  }

  promptDestination () {
    const themes = [];

    this.themeDirs.forEach(item => {
      themes.push({ name: item })
    });

    inquirer
      .prompt(
        [
          {
            type: 'list',
            name: 'themeDir',
            message: 'Please choose/confirm destination theme:',
            choices: themes
          }
        ]
       )
      .then(answers => {
        this.state.themeDir = answers.themeDir;
        this.promtType();
      });
  }

  templateExists (filePath) {
    try {
      if (fs.existsSync(filePath)) {
        return true;
      } else {
        return false;
      }
    } catch (err) {
      // console.error(err)
      console.log('should be false')
      // return false;
    }
  }

  // Not used at the moment.
  promptStrip () {
    inquirer.prompt(
      [
        {
          type: 'confirm',
          name: 'strip',
          message: ' Would you like to clean up the template?',
          default: true
        }
       ]
    ).then(answers => {
      console.log(answers);
    });
  }

  promtType () {
    inquirer.prompt([
      {
        type: 'list',
        message: 'Select template type',
        name: 'type',
        choices: [
          {
            name: 'field',
            disabled: this.templateExists(this.templatePath.field) ? false : 'template not found'
          },
          {
            name: 'paragraph',
            disabled: this.templateExists(this.templatePath.paragraph) ? false : 'template not found'
          }
        ]
       }
     ])
    .then(answers => {
      this.state.type = answers.type;
      this.promtTemplateName();
    });
  }

  promtTemplateName () {
    inquirer.prompt([
        {
          type: 'input',
          name: 'templateName',
          message: 'Filename of new template:',
          validate: value => {
            if (value.length) {
              return true;
            }

            return 'Not a valid template name.'
          }
        }
      ])
    .then(answers => {
      const regEx = /.html.twig/g
      this.state.templateName = answers.templateName.replace(regEx, '');
      this.saveTemplate();
    });
  }

  saveTemplate() {
    let srcPath = '';
    let srcSubDir = '';

    if (this.state.type === 'field') {
      srcPath = this.templatePath.field;
      srcSubDir = 'fields';
    } else {
      srcPath = this.templatePath.paragraph;
      srcSubDir = 'paragraphs';
    }

    if (!fs.existsSync(path.join(CWD, 'themes', this.state.themeDir, 'templates', srcSubDir))) {
      fs.mkdirSync(path.join(CWD, 'themes', this.state.themeDir, 'templates', srcSubDir))
    }

    fs.copyFile(srcPath, path.join(CWD, 'themes', this.state.themeDir, 'templates', srcSubDir, `${this.state.templateName}.html.twig`), fs.constants.COPYFILE_FICLONE, (err) => {
    if (err) {
      error.wb(err);
      process.exit(1);
    }
    success.wb(`Saved a new template file: ${path.join(CWD, 'themes', this.state.themeDir,'templates', srcSubDir ,this.state.templateName)}.htlm.twig`);
});
  }
}

const templater = new Templater();

templater.init();
